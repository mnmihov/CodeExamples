#ifndef SMARTPOINTER_H
#define SMARTPOINTER_H

/*
 * single type
 */
template <typename SPointerType>
class SmartPointer {
    SPointerType *pResource; // pointer to resource to manage
    size_t *references; // number of references
public:

    SmartPointer(SPointerType *resource) : pResource(resource), references(0) {
        references = new size_t();
        *references = 0;
        ++(*references);

    }

    SmartPointer(const SmartPointer<SPointerType> &sp) : pResource(sp.pResource), references(sp.references) {
        (*references)++;

    }

    SmartPointer operator=(SmartPointer<SPointerType> &sp) {
        pResource = sp.pResource;
        references = sp.references;
        ++(*references);
    }

    ~SmartPointer() {
        --(*references);

        if (*references == 0) {
            delete references;
            delete pResource;
        }

    }
};

/*
 * array specialization
 */
template <typename SPointerType>
class SmartPointer<SPointerType[]> {
    SPointerType *pResource; // pointer to resource to manage
    size_t *references; // number of references
public:

    SmartPointer(SPointerType *resource) : pResource(resource), references(0) {
        references = new size_t();
        *references = 0;
        ++(*references);
    }

    SmartPointer(const SmartPointer<SPointerType[]> &sp) : pResource(sp.pResource), references(sp.references) {
        (*references)++;
    }

    SmartPointer operator=(SmartPointer<SPointerType[]> &sp) {
        pResource = sp.pResource;
        references = sp.references;
        ++(*references);
    }

    ~SmartPointer() {
        --(*references);

        if (*references == 0) {
            delete references;
            delete[] pResource;
        }

    }
};


#endif /* SMARTPOINTER_H */
