#ifndef VECTOR_H
#define VECTOR_H
#include <cstring>
#include <stdexcept>

using namespace std;

template <typename T>
class Vector {
public:

    Vector() {
        size = 0;
        capacity = 1;
        pItems = new T[capacity];
    }

    ~Vector() {
        delete[] pItems;
    }

    void Push_back(T item) noexcept {
        if (size == capacity) {
            resizeAndCopy();
        }
        *(pItems + size++) = item;
    }

    void Pop_back() noexcept {
        --size;
    }

    void Insert(size_t position, T item) throw (std::range_error) {
        if (position > size) { //keep sequence of elements
            throw std::range_error("position is bigger than " + size); //
        }
        if (size == capacity) {
            //resize to fit
            resizeAndCopy();
        }

        if (size == position) {
            Push_back(item); //avoid unnecessary rearange
        } else {
            reArange(position); //make place for insert
            *(pItems + position) = item;
            ++size;
        }

    }

    void Remove(size_t position) throw (std::range_error) {
        if (position > (size - 1)) {
            throw std::range_error("no record at position: " + size - 1); //
        }

        if (position == size) {
            Pop_back(); //avoid unnecessary rearange
            return;
        }

        reArange(position, 0); //fill released index
        --size;
    }

    /*
     * rename from Remove to Removeif because of int colision
     * ‘void Vector<T>::Remove(T) [with T = int]’ cannot be overloaded
     */
    void RemoveIf(T item) throw (std::range_error) { // – delete all equal to item (operator ==)
        for (int i = 0; i < size; ++i) {
            if (*(pItems + i) == item) {
                Remove(i);
                --i;
            }
        }
    }

    T& operator[](size_t index) const {
        return *(pItems + index);
    }

    size_t length() const {
        return size;
    }


private:
    size_t size; // how many items we have in array
    size_t capacity; // for how many item we have memory
    T *pItems; // pointer to dinamicaly allocated array

    void resizeAndCopy() {
        // Make new array
        T *newArr = new T[2 * capacity];
        // Copy old array into new
        memcpy(newArr, pItems, capacity * sizeof (T));
        // Erase old array
        delete[] pItems;
        pItems = newArr;
        capacity = 2 * capacity;
    }

    void reArange(size_t toPos, int expand = 1) noexcept {
        if (expand == 1) { //expand
            for (int i = size; i >= toPos; --i) {
                *(pItems + i) = *(pItems + (i - 1));
            }
        } else { //shrink
            for (int i = toPos; i < size; ++i) {
                *(pItems + i) = *(pItems + (i + 1));
            }
        }
    }
};
#endif /* VECTOR_H */