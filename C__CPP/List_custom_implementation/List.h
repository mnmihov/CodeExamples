#ifndef LIST_H
#define LIST_H
#include <cstdlib>
#include <stdexcept>

using namespace std;

template <typename T>
class Node {
public:
    Node<T> *pNext; // pointer to next element
    Node<T> *pPrev; // pointer to previous element
    T value;

    Node() {
        pNext = NULL;
        pPrev = NULL;
    }
};

template <typename T>
class List {
public:

    List() {
        pRoot = NULL;
        pEnd = NULL;
        size = 0;
    }

    ~List() {
        this->Clear();
    }

    void push_back(T item) {// insert at the end

        Node<T> *tempPointer = new Node<T>();
        tempPointer->value = item;
        // check if this is first insert of item
        if (pRoot == NULL) {
            pRoot = tempPointer;
            pEnd = tempPointer;
            size++;
        } else {
            pEnd->pNext = tempPointer;
            tempPointer->pPrev = pEnd;
            pEnd = tempPointer;
            size++;
        }
    }

    T& pop_back() throw (std::length_error) { // remove from end
        if (pEnd == NULL) {
            throw std::length_error("List is epmpty!");
        }
        Node<T> * tempPointer;
        T *tmpData = new T(pEnd->value); // copy value 

        if (size == 1) { //only one item
            delete pEnd;
            pEnd = NULL;
            pRoot = NULL;
            --size;
            return *tmpData;
        }

        // redirect pointers
        (pEnd->pPrev)->pNext = NULL; //delete link to deleted item
        tempPointer = pEnd; //save address 
        pEnd = pEnd->pPrev; //redirect pEnd to previous element
        delete tempPointer; // delete last element
        --size;
        return *tmpData;

    }

    void push_front(T item) {
        Node<T> *tempPointer = new Node<T>();
        tempPointer->value = item;

        if (pRoot == NULL) { //empty list (size == 0)
            pRoot = tempPointer;
            pEnd = tempPointer;
        } else {
            // redirect pointers
            tempPointer->pNext = pRoot;
            pRoot->pPrev = tempPointer;
            pRoot = tempPointer;
        }
        ++size;
    }

    T& pop_front() throw (std::length_error) {
        if (pEnd == NULL) {
            throw std::length_error("List is epmpty!");
        }
        Node<T> * tempPointer;
        T *tmpData = new T(pEnd->value); // copy value 

        if (size == 1) { //only one item
            delete pRoot;
            pEnd = NULL;
            pRoot = NULL;
            --size;
            return *tmpData;
        }
        tempPointer = pRoot; //save address
        (pRoot->pNext)->pPrev = NULL; //delete link to previous
        pRoot = pRoot->pNext; //new root
        --size;
        delete tempPointer;
        return *tmpData;
    }

    // All next methods must throw excetion InvalidIndex 
    // when invalid position 

    void insertAt(size_t position, T item) throw (std::out_of_range) {
        if (position > size) {//size is valid index for record
            throw std::out_of_range("Invalid index!");
        }

        if (position == 0) {
            push_front(item);
            return;
        }

        if (position == size) {
            push_back(item);
            return;
        }

        Node<T> *tmpData = new Node<T>();
        tmpData->value = item;
        findMeAPosAndLink(tmpData, position);
        ++size;

    }

    T& removeAt(size_t position) { // returns and remove  item at position
        if (position > (size - 1)) {
            throw std::out_of_range("Invalid index!");
        }

        Node<T>* tmpPtr;
        T* tmpData;

        tmpPtr = findMyPos(position);

        tmpData = &tmpPtr->value;

        //relink
        (tmpPtr->pNext)->pPrev = tmpPtr->pPrev;
        (tmpPtr->pPrev)->pNext = tmpPtr->pNext;
        --size;
        delete tmpPtr;
        return *tmpData;

    }

    T operator[](size_t idx) { // return value of element on idx position
        if (idx > (size - 1)) {
            throw std::out_of_range("Invalid index!");
        }

        Node<T>* tmpPtr;
        tmpPtr = findMyPos(idx);
        return tmpPtr->value;

    }

    void Clear() { // remove all elements
        Node<T>* tmpPtr = pRoot;
        //nat
        while (tmpPtr == NULL) {
            delete tmpPtr;
            tmpPtr = tmpPtr->pNext;
        }
        pRoot = NULL;
        pEnd = NULL;
        size = 0;
    }

    size_t length() const {
        return size;
    }

private:
    size_t size; // how many items we have in list
    Node<T> *pRoot; // pointer to first element in list
    Node<T> *pEnd; // pointer to last element in list

    Node<T> * findMyPos(size_t pos) {//return pointer to node at position X;
        size_t endIndex = size - 1;
        Node<T>* tmpPoint;
        //optimize search of index
        //return (1) if closer to the end
        int whereToStart = ((endIndex - pos) <= (pos)) ? 1 : 0;

        if (whereToStart == 1) { //search from back
            tmpPoint = pEnd;
            for (int i = endIndex; i > pos; --i) {
                tmpPoint = tmpPoint->pPrev;
            }
        } else { //search from front
            tmpPoint = pRoot;
            for (int i = 0; i < pos; ++i) {
                tmpPoint = tmpPoint->pNext;
            }
        }

        return tmpPoint;

    }

    //set element to position

    void findMeAPosAndLink(Node<T>* ptr, size_t pos) {
        size_t endIndex = size - 1;
        Node<T>* tmpPoint;
        //optimize search of index
        //return (1) if closer to the end
        int whereToStart = ((endIndex - pos) <= pos) ? 1 : 0;

        if (whereToStart == 1) { //search from back
            tmpPoint = pEnd;
            for (int i = endIndex; i > pos; --i) {
                tmpPoint = tmpPoint->pPrev;
            }


        } else { //search from front
            tmpPoint = pRoot;
            for (int i = 0; i < pos; ++i) {
                tmpPoint = tmpPoint->pNext;
            }
        }

        ptr->pNext = tmpPoint;
        ptr->pPrev = tmpPoint->pPrev;

        (tmpPoint->pPrev)->pNext = ptr;
        tmpPoint->pPrev = ptr;


    }

};


#endif /* LIST_H */
