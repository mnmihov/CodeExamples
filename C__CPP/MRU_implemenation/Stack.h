#ifndef STACK_H
#define STACK_H
#include <cstdlib>

template<class T, int s_size = 10 >
class Stack {
public:

    Stack() : stackData(new T[s_size]) {
        itTail = stackData;
        itHead = stackData;
    }

    virtual ~Stack() {
        delete []stackData;
    }

    void push(const T elem) noexcept {
        *itTail = elem;
        moveFront();
    }

    T pop() noexcept {
        if (size() == 0) {
            return NULL;
        } else {
            moveBack();
            return *itTail;
        }
    }

    int head() const noexcept {
        return (itHead - stackData);
    }

    int tail() const noexcept {
        return (itTail - stackData);
    }

    int size() const noexcept {
        int size = (itHead == itTail) ? 0 : (itTail - itHead);
        return (size < 0) ? s_size : size;
    }
private:
    T * const stackData;
    T *itHead;
    T *itTail;

    void moveFront() noexcept {
        if (itTail == (stackData + s_size)) {
            itTail = stackData;
            if (itHead == itTail) {
                ++itHead;
            }

        } else if (itHead == (stackData + s_size)) {
            itHead = stackData;
            ++itTail;
        } else {

            if (itHead - itTail != 1) {
                ++itTail;
            } else {
                ++itTail;
                ++itHead;
            }
        }
    }

    void moveBack() noexcept {
        if (itTail == stackData) {
            itTail = (stackData + s_size);

        } else {
            --itTail;
        }
    }
};


#endif /* STACK_H */
